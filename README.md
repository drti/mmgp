- [1. Description](#1-description)
- [2. Getting started](#2-getting-started)
  - [Remark:](#remark)
- [3. Documentation](#3-documentation)

---

## 1. Description

MMGP is the name of the numerical method proposed at SafranTech, the research center of [Safran group](https://www.safran-group.com/), in the paper: Mesh Morphing Gaussian Process-based machine learning method for regression of physical problems under non-parameterized geometrical variability ([arXiv preprint](https://arxiv.org/abs/2305.12871), [neurips 2023](https://neurips.cc/virtual/2023/poster/72423)). In particular, this library provided an example reproducing the numerical experiments  associated to the dataset <tt>`Tensile2d`</tt>.

MMGP also refers to this library, which implements this methods.

Please consider citing the paper when using this library:
```bibtex
@article{mmgp2023,
      title={MMGP: a Mesh Morphing Gaussian Process-based machine learning method for regression of physical problems under non-parameterized geometrical variability},
      author={Casenave, F. and Staber, B. and Roynard, X.},
      journal={arXiv preprint arXiv:2305.12871},
      year={2023}
}
```

---

## 2. Getting started

To use the library, the simplest way is to install the conda-forge package:

```bash
conda install -c conda-forge mmgp
```

This package comes with the default scikit-learn and [`gpy`](https://github.com/SheffieldML/GPy) GP backend. A gpjax backend is available in `src/mmgp/backends` and requires installing [`gpjax`](https://github.com/JaxGaussianProcesses/GPJax).


### Remark:

* The multioutput posteriori sampling is not available in the gpjax backend.

<!-- * A known issue of [`gpy`](https://github.com/SheffieldML/GPy) is a [`numpy`](https://github.com/numpy/numpy) compatibility restricted to versions less or equal that 1.23.5. As long as this issue persists, MMGP with gpy backend requires a manual install of [`Muscat`](https://gitlab.com/drti/muscat), [`plaid`](https://gitlab.com/drti/plaid) and `mmgp`, since none of this libraries has been packaged with `numpy`<=1.23.5. -->


To install from the sources, consider using the provided conda yaml files for configuring a compatible environment:

```bash
conda env create -f '<environment_you_need>.yml'
```

Then:

```bash
pip install -e .
```


To check the installation, you can run:
```bash
cd tests
python -m pytest
```

Consider exploring the examples folder, where the entry `init_dataset_location` in the file `configuration.yaml` should be updated.

---

## 3. Documentation

A documentation is available in [readthedocs](https://mmgp.readthedocs.io/en/latest/).
