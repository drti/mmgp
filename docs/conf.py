# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import datetime
import os
import sys

basedir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, basedir)
sys.path.insert(0, os.path.join(basedir, "src/mmgp"))
print(sys.path)

# -- Project information -----------------------------------------------------
root_doc = 'index'  # default is already <index>
project = 'MMGP'
copyright = '2023-{}, Safran'.format(datetime.date.today().year)
author = 'Safran'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
    'sphinx.ext.todo',
    'sphinx.ext.intersphinx',
    'sphinx.ext.doctest',
    'sphinx.ext.coverage',
    'sphinx.ext.inheritance_diagram',
    'sphinx.ext.ifconfig',
    'sphinx.ext.duration',
    'sphinx.ext.extlinks',
    'sphinx.ext.napoleon',
    'sphinx.ext.graphviz',
    'myst_parser',
    'sphinx.ext.autosummary',
    # "sphinx_math_dollar",
    'nbsphinx',
    "sphinx.ext.mathjax",
]

mathjax3_config = {
    'TeX': {'equationNumbers': {'autoNumber': 'AMS', 'useLabelIds': True}},
}

numfig = True
# ---------------------------------------------------------#
# autosummary options
autosummary_generate = True

# -----------------------------------------------------------------------------#
# autoapi options :
# https://sphinx-autoapi.readthedocs.io/en/latest/tutorials.html#setting-up-automatic-api-documentation-generation
extensions.append('autoapi.extension')
autoapi_dirs = ['../src/mmgp']
autoapi_type = 'python'
autoapi_options = ['show-inheritance', 'show-module-summary', 'undoc-members']
# 'members': Display children of an object
# 'inherited-members': Display children of an object that have been inherited from a base class.
# 'undoc-members': Display objects that have no docstring
# 'private-members': Display private objects (eg. _foo in Python)
# 'special-members': Display special objects (eg. __foo__ in Python)
# 'show-inheritance': Display a list of base classes below the class signature.
# 'show-inheritance-diagram': Display an inheritance diagram in generated class documentation. It makes use of the sphinx.ext.inheritance_diagram extension, and requires Graphviz to be installed.
# 'show-module-summary': Whether to include autosummary directives in generated module documentation.
# 'imported-members': Display objects imported from the same top level package or module. The default module template does not include imported objects, even with this option enabled. The default package template does.
# autodoc_typehints = "signature"
autoapi_keep_files = True
# autoapi_add_toctree_entry = False
autoapi_add_toctree_entry = True
autoapi_python_class_content = 'both'  # default is 'class'
# autoapi_mock_imports = ["plaid", "Muscat"]
# autoapi_member_order = 'bysource'

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

source_suffix = '.rst'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.

# html_theme = 'sphinx_rtd_theme'
# html_css_files = ['custom.css']
# html_title = "MMGP"
html_theme = 'furo'
html_logo = "images/logoMMGP.jpg"

# cf https://pradyunsg.me/furo/customisation/edit-button/
html_theme_options = {
    "source_edit_link": "https://gitlab.com/drti/mmgp",
    "source_branch": "main",
    "source_directory": "docs/",
    # 'logo_only': True,
}

github_url = 'https://gitlab.com/drti/mmgp'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# A list of paths that contain extra files not directly related to the documentation,
# such as robots.txt or .htaccess.
# Relative paths are taken as relative to the configuration directory.
# They are copied to the output directory.
# They will overwrite any existing file of the same name.
# As these files are not meant to be built, they are automatically
# excluded from source files.
html_extra_path = ['_extra']


def skip_logger_attribute(app, what, name, obj, skip, options):
    if what == 'data' and "logger" in name:
        print(f"WILL SKIP: {what=}, {name=}")
        skip = True
    return skip


def setup(sphinx):
    sphinx.connect("autoapi-skip-member", skip_logger_attribute)
