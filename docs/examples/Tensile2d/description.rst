Tensile2d
=========

The dataset ``Tensile2d`` is available in PLAID format `here <https://zenodo.org/records/10124594>`_.
PLAID (Physics Informed AI Datamodel) is a library proposing an implementation for a datamodel tailored for
AI and ML learning of physics problems.
As for MMGP, PLAID has been developped at SafranTech.
The code is hosted on `Safran Gitlab <https://gitlab.com/drti/plaid>`_, and a documentation is available
on `readthedocs <https://plaid-lib.readthedocs.io/>`_.

The dataset ``Tensile2d`` is used as numerical experiment in the
`MMGP paper <https://mmgp.readthedocs.io/en/latest/pages/paper.html>`_.
The machine learning problem is presented in the PLAID documention, under
`data challenges <https://plaid-lib.readthedocs.io/en/latest/source/data_challenges/tensile2d.html>`_.

Finally, the files required to run the MMGP method on the dataset are available in the
`MMGP's repository example folder <https://gitlab.com/drti/mmgp/-/tree/main/examples/Tensile2d>`_.
In particular, refer to the `example readme <https://gitlab.com/drti/mmgp/-/blob/main/examples/Tensile2d/README.md>`_.
We mention here that the example is set with the scikit-learn backend for ease of use, although
the GPy backend was used in the `MMGP paper <https://mmgp.readthedocs.io/en/latest/pages/paper.html>`_.
