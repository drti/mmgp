#!/bin/bash

#---# Clean
rm -rf _build
rm -rf pages/additional
rm -rf autoapi
# rm -rf api_reference
# mkdir -p pages/additional
# mkdir -p _extra

#---# Copy coverage in local
# rm -rf _extra/coverage
# cp -r ../public/coverage _extra/coverage

# #---# Copy notebooks in local
# rm -rf pages/additional/notebooks
# mkdir -p pages/additional/notebooks
# cp ../notebooks/*.ipynb pages/additional/notebooks
# rm -rf notebooks
# mkdir -p notebooks
# cp -r ../examples/notebooks .

#---# Verbose BEFORE
echo "";echo "#---# ls -lAh pages/"
ls -lAh pages/
echo "";echo "#---# ls -lAh notebooks/"
ls -lAh notebooks/
# echo "";echo "#---# ls -lAh pages/additional/*"
# ls -lAh pages/additional/*
# echo "";echo "#---# ls -lAh api_reference/"
# ls -lAh api_reference/
# cat api_reference/*/module*

#---# Generate doc site
# export PYDEVD_DISABLE_FILE_VALIDATION=1
make html

#---# Verbose AFTER
echo "";echo "#---# ls -lAh"
ls -lAh *
echo "";echo "#---# ls -lAh _build/html/*"
ls -lAh _build/html/*
echo "";echo "#---# ls -lAh _build/html/autoapi"
ls -lAh _build/html/autoapi
# echo "";echo "#---# ls -lAh _build/html/reports"
# ls -lAh _build/html/reports

rm -rf notebooks/*/data

#---# Post
rsync -av --delete-after _build/html/* ../public
