Welcome to MMGP
===============

MMGP is the name of the numerical method proposed at SafranTech,
the research center of `Safran group <https://www.safran-group.com/>`_,
in the paper: Mesh Morphing Gaussian Process-based machine learning method
for regression of physical problems under non-parameterized geometrical variability
(`arXiv preprint <https://arxiv.org/abs/2305.12871>`_,
`neurips 2023 <https://arxiv.org/abs/2305.12871>`_).
MMGP also refers to this library, which implements this methods.

The code is hosted on `Safran Gitlab <https://gitlab.com/drti/mmgp>`_.


.. toctree::
   :maxdepth: 1
   :caption: Introduction

   pages/paper.md

.. toctree::
   :maxdepth: 1
   :caption: Getting started

   pages/getting_started

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: API Documentation

   Autoapi <autoapi/index>
   Examples <pages/examples.rst>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
