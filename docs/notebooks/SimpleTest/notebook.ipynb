{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Simple workflow"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this simple example, we detail each stage of the MMGP workflow. Consider the following representation of the inference workflow below, that we will refer to in this notebook:\n",
    "![MMGPWorkflow](../../pages/images/mmgp_workflow.png)\n",
    "*Figure 1*: MMGP inference workflow for fields outputs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Load configuration\n",
    "\n",
    "The configuration yaml file is loaded, as well as the problem definition from the PLAID format. These setting are then printed to the console."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mmgp.utils import read_configuration, read_problem, print_setting\n",
    "\n",
    "configuration = read_configuration(config_file=\"configuration.yaml\")\n",
    "problem = read_problem(configuration)\n",
    "print_setting(configuration, problem)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "TODO: continue the list\n",
    "\n",
    "- ``init_dataset_location`` indicates where the dataset in PLAID format is located, containing the definition of the learning problem, and all the samples, each contain a physics configuration (see *Figure 2*).\n",
    "- ``generated_data_folder`` is the location of the folder where the MMGP method will save the produced data. Here, it a the dolfer ``data`` in the current working director."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![InitDataset](../../pages/images/SimpleTestInitDatabase.png)\n",
    "*Figure 2*: Initial database, representation of ``field_1``; (top) training set, (bottom) testing set. We notice a variety of shape and meshes sizes, as well as fields magnitudes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we inspect the current working directory, it contains only the ``configuration.yaml`` file and the present notebook:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "os.listdir()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Preprocess database"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following ``pre_process`` function morphs the meshes $\\mathcal{M}^i$ of all samples onto the common mesh $\\overline{\\mathcal{M}}_c$ of the reference shape $\\overline{\\Omega}$ and computes finite element interpolations on it. The morphing algorithm is chosen following the configuration ``morphing\\algo`` from the ``configuration.yaml`` file, with provided options.\n",
    "\n",
    "The folder ``data`` is created and populated with three folders (see *Figure 1*):\n",
    "\n",
    "- ``FEInterpolationOperators``: precomputation of finite element interpolation operators between each sample mesh and the common mesh of the reference shape, both direct and inverse,\n",
    "- ``SimpleTest_morphed``: dataset with morphed meshes $\\overline{\\mathcal{M}}^i$ and transported fields $\\overline{\\mathcal{Z}}^i_{\\ell}$ and $\\overline{\\mathcal{U}}^i_k$ (see *Figure 3*),\n",
    "- ``SimpleTest_morphed_and_projected``: dataset all fields $\\widetilde{\\mathbf{Z}}^i_{\\ell}$ and $\\widetilde{\\mathbf{U}}^i_k$ interpolated onto the common mesh $\\overline{\\mathcal{M}}_c$ (see *Figure 4*)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mmgp.preprocessing import pre_process\n",
    "\n",
    "pre_process(configuration)\n",
    "os.listdir(\"data\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![MorphedDataset](../../pages/images/SimpleTestMorphedDatabase.png)\n",
    "*Figure 3*: Morphed database, representation of ``field_1``; (top) training set, (bottom) testing set. The meshes $\\mathcal{M}^i$ have been morphed, and the fields transported. The morphed meshes $\\overline{\\mathcal{M}}^i$ are discretization of the same reference shape (the unit disk $\\overline{\\Omega}$), but are still different"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![MorphedAndInterpolatedDataset](../../pages/images/SimpleTestMorphedProjectedDatabase.png)\n",
    "*Figure 4*: Morphed and interpolated database, representation of ``field_1``; (top) training set, (bottom) testing set. The fields have been interpolated onto the common mesh $\\overline{\\mathcal{M}}_c$ of the reference shape $\\overline{\\Omega}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Train"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following ``train`` function trains the Gaussian process regressor using data from the training set. The GP algorithm is chosen following the configuration ``regression\\algo`` from the ``configuration.yaml`` file, with provided options. The inputs and outputs of the GP are taken from the ``problem_definition`` of the init PLAID object. The dimensionality of the fields input and output interpolated on the common mesh $\\overline{\\mathcal{M}}_c$, respectivement $\\widetilde{\\mathbf{Z}}^i_{\\ell}$ and $\\widetilde{\\mathbf{U}}^i_k$, is reduced in this stage to obtain $\\widehat{\\mathbf{Z}}_{\\ell}$ and $\\widehat{\\mathbf{U}}^i_k$, following the configurations ``dimensionality_reduction\\algo`` from the ``configuration.yaml`` (see *Figure 1*).\n",
    "\n",
    "The file ``trainingRegressionData.pkl`` is added in the folder ``data`` to save the trained GP and corresponding scalers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mmgp.processing import train\n",
    "\n",
    "train_data = train(configuration, problem)\n",
    "os.listdir(\"data\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Infer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following ``infer`` function computes the prediction using the trained GP, for both the training and testing data.\n",
    "\n",
    "The file ``allPredictedData.pkl`` is added in the folder ``data`` to save the predicted data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mmgp.processing import infer\n",
    "\n",
    "predicted_data = infer(configuration, problem)\n",
    "os.listdir(\"data\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Compute metrics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following ``compute_metrics`` function computes rRMSE and R2 for all output scalars and fields, for both the training and testing data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mmgp.postprocessing import compute_metrics\n",
    "\n",
    "metrics = compute_metrics(configuration, problem)\n",
    "\n",
    "import rich\n",
    "rich.print(metrics)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 6. Export predictions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following ``export_predictions`` saves the predicted data on disk in the PLAID format.\n",
    "\n",
    "The folder ``SimpleTest_predicted`` is added in the folder ``data``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mmgp.postprocessing import export_predictions\n",
    "\n",
    "predicted_dataset = export_predictions(configuration, problem)\n",
    "os.listdir(\"data\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
