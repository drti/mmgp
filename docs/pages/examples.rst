Examples
========

You can find here detailed examples of MMGP's use, some presented in jupyter notebooks.

.. toctree::
   :glob:
   :maxdepth: 3

   ../notebooks/SimpleTest/notebook
   ../examples/Tensile2d/description
