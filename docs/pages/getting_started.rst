Install guide
=============

Conda
-----

To use the library, the simplest way is to install the conda package:

.. code::

    conda install -c conda-forge mmgp

This package comes with the default scikit-learn GP backend.
Other backends are available in `src/mmgp/backends` and require installing additional dependencies.
For the gpjax and gpy backends, `gpjax`_, and `gpy`_ are respectively required


Remark
------

* The multioutput posteriori sampling is not available in the gpjax backend.

.. * A known issue of `gpy`_ is a `numpy`_ compatibility restricted to versions less or equal that 1.23.5. As long as this issue persists,
..   MMGP with gpy backend requires a manual install of `Muscat`_,
..   `plaid`_ and `mmgp`, since none of this libraries has been packaged with `numpy`_ version less or equal than 1.23.5.


From source
-----------

To install from source, clone this repository, then add the package to your **PYTHONPATH**
or simply do

.. code::

    cd mmgp
    pip install -e .

All the requirements are listed in the files `conda_dev_env.yml` and `conda_minimal.yml`:
use the first one is you plan to run the tests or compile the documentation.

Conda environement can be created as follows:

.. code::

    conda env create -n mmgp -f conda_dev_env.yml


Activate the new environment:

.. code::

    conda activate mmgp

And test if it is working properly:

.. code::

    python -c "import mmgp; print(dir(mmgp))"

To check the installation, if you used the `conda_dev_env.yml` configuration file,
you can run:

.. code::

    cd tests
    python -m pytest
    cd ../examples
    python run_examples.py --backend scikit-learn
    python run_examples.py --backend GPy


.. _gpjax: https://github.com/JaxGaussianProcesses/GPJax
.. _gpy: https://github.com/SheffieldML/GPy
.. _numpy: https://github.com/numpy/numpy
.. _Muscat: https://gitlab.com/drti/muscat
.. _plaid: https://gitlab.com/drti/plaid
