(mmgp_paper_ref)=
# MMGP Paper

MMGP is the name of the numerical method proposed at SafranTech, the research center of [Safran group](https://www.safran-group.com/), in the paper: Mesh Morphing Gaussian Process-based machine learning method for regression of physical problems under non-parameterized geometrical variability ([arXiv preprint](https://arxiv.org/abs/2305.12871), [neurips 2023](https://neurips.cc/virtual/2023/poster/72423)). In particular, this library provided an example reproducing the numerical experiments  associated to the dataset <tt>`Tensile2d`</tt>.

MMGP also refers to this library, which implements this methods.

Please consider citing the paper when using this library:
```bibtex
@article{mmgp2023,
      title={MMGP: a Mesh Morphing Gaussian Process-based machine learning method for regression of physical problems under non-parameterized geometrical variability},
      author={Casenave, F. and Staber, B. and Roynard, X.},
      journal={arXiv preprint arXiv:2305.12871},
      year={2023}
}
```

## Abstract

When learning simulations for modeling physical phenomena in industrial designs, geometrical variabilities are of prime interest. While classical regression techniques prove effective for parameterized geometries, practical scenarios often involve the absence of shape parametrization during the inference stage, leaving us with only mesh discretizations as available data.
Learning simulations from such mesh-based representations poses significant challenges, with recent advances relying heavily on deep graph neural networks to overcome the limitations of conventional machine learning approaches. Despite their promising results, graph neural networks exhibit certain drawbacks, including their dependency on extensive datasets and limitations in providing built-in predictive uncertainties or handling large meshes. In this work, we propose a machine learning method that do not rely on graph neural networks. Complex geometrical shapes and variations with fixed topology are dealt with using well-known mesh morphing onto a common support, combined with classical dimensionality reduction techniques and Gaussian processes.
The proposed methodology can easily deal with large meshes without the need for explicit shape parameterization and provides crucial predictive uncertainties, which are essential for informed decision-making. In the considered numerical experiments, the proposed method is competitive with respect to existing graph neural networks, regarding either training efficiency and accuracy of the predictions.


## MMGP Workflow

![MMGP Workflow](images/mmgp_workflow.png)
*Illustration of the MMGP inference workflow for the prediction of an output field of interest*

## Outlook


Our proposed method presents an innovative approach to approximating field and scalar quantities of interest within the context of solving complex physics problems for design optimization. Our work introduces two key contributions: firstly, the utilization of mesh morphing pretreatment in conjunction with finite element interpolation, and secondly, the incorporation of shape embedding through dimensional reduction of coordinates, treating them as continuous fields over the geometric support. These innovations alleviate the machine learning task from the challenges of handling variable-sized samples and the need to learn implicit shape embedding. By reducing the dimensionality of inputs and outputs, our approach allows for the application of efficient Gaussian process regression. Notably, our MMGP model exhibits several key advantages. It can seamlessly handle very large meshes, is amenable to efficient CPU training, is fairly interpretable, demonstrates high accuracy in our experimental results, and provides readily available predictive uncertainties.

Future works will explore the extension of our method to accommodate time-dependent quantities of interest and investigate the optimization of the morphing process to enhance data compression and overall performance. Our research opens exciting avenues for advancing the capabilities of machine learning in the realm of physics-based design optimization.
