import numpy as np
from BasicTools.Bridges.CGNSBridge import MeshToCGNS
from BasicTools.Containers import UnstructuredMeshCreationTools as UMCT
from plaid.containers.dataset import Dataset
from plaid.containers.sample import Sample
from plaid.problem_definition import ProblemDefinition


# parametrization of samples:
# - the first entry indicates a geometrical parameter (length of the size for the square and diameter for the disk),
# - the second entry is an input of the ML problem (magnitude of the field of interest),
# - the third entry is the number of elements in one direction
samples_infos={
    0:(1., 1., 10),
    1:(1.1, 0.8, 6),
    2:(0.9, 1.4, 8),
    3:(0.8, 0.4, 14),
    4:(1.05, 1.2, 11),
    5:(0.85, 0.9, 12),
    6:(0.95, 1.1, 9)
}

# ---#
samples = []
for i in range(7):

    sample_info = samples_infos[i]
    l = sample_info[0]
    a = sample_info[1]
    N = sample_info[2]

    spac = l/(N-1.)
    mesh = UMCT.CreateSquare(dimensions=[N,N], origin=[0.,0.], spacing=[spac,spac], ofTriangles=True)

    field_1 = a*np.sin(mesh.nodes[:,0])
    field_2 = a*np.sin(mesh.nodes[:,1])

    CGNSmesh = MeshToCGNS(mesh)

    sample = Sample()
    sample.add_tree(CGNSmesh)
    sample.add_scalar('a', a)
    sample.add_scalar('l', l)
    sample.add_field('field_1', field_1, "Zone", "Base_2_2")
    sample.add_field('field_2', field_2, "Zone", "Base_2_2")

    samples.append(sample)

# ---#
infos = {
    "legal": {
        "owner": "Owner",
        "license": "Licence"},
    "data_production": {
        "type": "Test",
        "physics": "test data for mmgp library",
        "simulator": "test"}
}

# ---#
dataset = Dataset()
dataset.add_samples(samples)
dataset.set_infos(infos)

dataset._save_to_dir_("dataset")

# ---#
split = {
    'train': [0, 1, 2, 3],
    'test': [4, 5, 6],
}

# ---#
problem_definition = ProblemDefinition()
problem_definition.add_inputs(['mesh', 'a'])
problem_definition.add_outputs(['field_1', 'field_2', 'l'])
problem_definition.set_task('regression')
problem_definition.set_split(split)

problem_definition._save_to_dir_("problem_definition")
