# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#
#
import time
import argparse
from mmgp.utils import read_configuration, read_problem

# python run.py --preprocess --train scikit-learn --infer --compute_metrics --export_predictions
# the backend is chosen when adding its name after --train

parser = argparse.ArgumentParser()
parser.add_argument("--preprocess", action="store_true", help="Perform the preprocessing steps morphing and finite element interpolation")
parser.add_argument("--train", nargs='?', const='scikit-learn', type=str, help="Train the latent Gaussian process using a GP algo among 'GPy' and 'scikit-learn'")
parser.add_argument("--infer", action="store_true", help="Predict with the trained model")
parser.add_argument("--compute_metrics", action="store_true", help="Compute evaluation metrics")
parser.add_argument("--export_predictions", action="store_true", help="Export the predictions in PLAID format")
args = parser.parse_args()

print("----------------------------------------------")

configuration = read_configuration(config_file="configuration-"+args.train+".yaml")
print("Using configuration-"+args.train+".yaml")
problem = read_problem(configuration)

print("----------------------------------------------")
print(" MMGP workflow")
print("----------------------------------------------")

if args.preprocess:
    from mmgp.preprocessing import pre_process
    print(" Preprocess starting ...")
    start = time.time()

    pre_process(configuration)

    print("... preprocess done in "+str(time.time() - start)+" s")
else:
    print(" Preprocess bypassed")


print("----------------------------------------------")


if args.train:
    from mmgp.processing import train
    print(" Train starting ...")
    start = time.time()

    train_data = train(configuration, problem)

    print("... train done in "+str(time.time() - start)+" s")
else:
    print(" Train bypassed")


print("----------------------------------------------")


if args.infer:
    from mmgp.processing import infer
    print(" Inference starting ...")
    start = time.time()

    predicted_data = infer(configuration, problem)

    print("... inference done in "+str(time.time() - start)+" s")
else:
    print(" Inference bypassed")


print("----------------------------------------------")


if args.compute_metrics:
    from mmgp.postprocessing import compute_metrics
    print(" Computation of metrics starting ...")
    start = time.time()

    metrics = compute_metrics(configuration, problem)

    print("... computation of metrics done in "+str(time.time() - start)+" s")
else:
    print(" Computation of metrics bypassed")


print("----------------------------------------------")
print(" Export of the predictions in PLAID format")
print("----------------------------------------------")


if args.export_predictions:
    from mmgp.postprocessing import export_predictions
    print(" Exporting predictions ...")
    start = time.time()

    predicted_dataset = export_predictions(configuration, problem)

    print("... predictions exported in "+str(time.time() - start)+" s")
else:
    print(" Exporting predictions bypassed")


print("----------------------------------------------")
