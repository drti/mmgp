### Tensile2d example

This folder contains a step by step example that can be ran with the `run.py` script.

Please first update the `init_dataset_location` entry in the `configuration.yaml`
file, by refering the to location of the `Tensile2d` in PLAID format,
available [hare](https://zenodo.org/records/10124594).

```bash
python run.py --help
usage: run.py [-h] [--preprocess] [--train] [--infer] [--scalar_plots] [--complete_example_inference] [--export_predictions]

options:
  -h, --help            show this help message and exit
  --preprocess          Perform the preprocessing steps morphing and finite element interpolation
  --train               Train the latent Gaussian process
  --infer               Predict with the trained model
  --scalar_plots        Plot some results
  --complete_example_inference
                        Run the complete inference example
  --export_predictions  Export the predictions in PLAID format
```

To execute the full example, run the following command:

```bash
python run.py --preprocess --train --infer --scalar_plots --complete_example_inference --export_predictions
```

Note that each step can be ran individually, under the condition that the previous steps have been already executed.
