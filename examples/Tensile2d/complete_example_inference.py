# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#
#

import numpy as np
import os, pickle

from Muscat.Bridges.CGNSBridge import CGNSToMesh

from plaid.containers.dataset import Dataset

from mmgp.morphing import Morphing
from mmgp.FE_utils import compute_FE_projection_operators

from mmgp.utils import read_configuration, read_problem


# sample number from testing set
test_num = 33

# Load data
configuration = read_configuration(config_file="configuration.yaml")
problem = read_problem(configuration)

if configuration["regression"]["uncertainties"]:
    regressor_num = configuration["regression"]["reference_regressor"]
    nMonteCarloSamples = configuration["regression"]["number_Monte_Carlo_samples"]
else:
    regressor_num = 0
    nMonteCarloSamples = 1

zone_name = configuration["zone_name"]
base_name = configuration["base_name"]
test_set_name = configuration["test_set"]

# Load the problem and dataset

testing_set = problem.get_split(test_set_name)
testing_index = testing_set[test_num]

dataset = Dataset()
dataset._load_from_dir_(configuration['init_dataset_location']+os.sep+"dataset",ids=[testing_index])

inScalarsNames  = problem.in_scalars_names
outScalarsNames = problem.out_scalars_names
outFieldsNames  = problem.out_fields_names

# Load the data constructing during the training workflow
file = open(configuration["generated_data_folder"]+os.sep+"trainingRegressionData.pkl",'rb')
trainingRegressionData = pickle.load(file)
file.close()

podProjInCoordFields = trainingRegressionData["podProjInCoordFields"]
scalerInScalar = trainingRegressionData["scalerInScalar"]
scalerX = trainingRegressionData["scalerX"]

podProjOutFields = trainingRegressionData["podProjOutFields"]
scalerYFields = trainingRegressionData["scalerYFields"]

scalerYScalars = trainingRegressionData["scalerYScalars"]

fieldsRegressors = trainingRegressionData["fieldsRegressorsList"][regressor_num]
scalarsRegressors = trainingRegressionData["scalarsRegressorsList"][regressor_num]

# Compute morphing of sample mesh

sample = dataset[testing_index]
cgnsMesh = sample.get_mesh()
mesh = CGNSToMesh(cgnsMesh)

morphing_algo = configuration["morphing"]["algo"]
morphing_options = configuration["morphing"]["options"]
morphing = Morphing(algo=morphing_algo, options=morphing_options)
morphedMesh = morphing.transform(mesh)


# Load common morphed mesh
commonMeshIndex = configuration["common_mesh_index"]


morph_dataset = Dataset()
morph_dataset._load_from_dir_(configuration["generated_data_folder"]+os.sep+configuration["case_name"]+"_morphed_and_projected"+os.sep+"dataset",ids=[commonMeshIndex])
cgns_mesh = morph_dataset[commonMeshIndex].get_mesh()
commonMorphedMesh = CGNSToMesh(cgns_mesh)


# Compute the two FE projection operators

projOperator, invProjOperator = compute_FE_projection_operators(
    morphedMesh, commonMorphedMesh, verbose=True)


# Compute inputs for inference

# - input scalars, with rescaling
inScalars = dataset.get_scalars_to_tabular(
    scalar_names=inScalarsNames,
    sample_ids=[testing_index],
    as_nparray=True)
rescaledInScalar = scalerInScalar.transform(inScalars)

# - input projected coordinates field, with dimensionality reduction
nNodesCommonMorphedMesh = commonMorphedMesh.GetNumberOfNodes()
physicalDimension = mesh.GetPointsDimensionality()

projInCoordField = np.array([])
for j in range(physicalDimension):
    coord_field = mesh.nodes[:, j]
    projInCoordField = np.hstack(
        (projInCoordField, projOperator.dot(coord_field)))
projInCoordField = projInCoordField.reshape(1, -1)

reducedProjInCoordFields = podProjInCoordFields.transform(projInCoordField)

# - create the low-dimensional input by concatenating the two previous inputs, and rescale
X = np.hstack((reducedProjInCoordFields, rescaledInScalar))
rescaledX = scalerX.transform(X)
