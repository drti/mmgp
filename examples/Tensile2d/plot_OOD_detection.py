# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#
#
import os
import pickle

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors
import shutil

from mmgp.utils import remove_file, read_configuration, read_problem

"""
Fig.23

"""
if os.path.isdir("plots") == False:
    os.mkdir("plots")
else:
    remove_file("plots/OOD_detection_histo.png")


# Load data
configuration = read_configuration(config_file="configuration.yaml")
problem = read_problem(configuration)

if configuration["regression"]["uncertainties"]:
    regressor_num = configuration["regression"]["reference_regressor"]
else:
    regressor_num = 0

# Load the predicted fields data from chosen GP
file = open(configuration["generated_data_folder"]+os.sep+"allPredictedData.pkl",'rb')
predictedData = pickle.load(file)[regressor_num]
file.close()

predictedOutScalarsVariance = predictedData["predictedOutScalarsVariance"]


# Load the problem definition
OOD_set     = problem.get_split("OOD")

test_set_name = configuration["test_set"]
testing_set = problem.get_split(test_set_name)

outScalarsNames = problem.out_scalars_names

logPredictedOutScalarsVarianceTest = {}
logPredictedOutScalarsVarianceOOD = {}
for sname in outScalarsNames:
    logPredictedOutScalarsVarianceTest[sname] = np.log10(
        np.array(predictedOutScalarsVariance[sname])[testing_set])
    logPredictedOutScalarsVarianceOOD[sname] = np.log10(
        np.array(predictedOutScalarsVariance[sname])[OOD_set])


if os.path.isfile("data/predictedScalarsDataOODParam.pkl") == False:
    print("file data/predictedScalarsDataOODParam.pkl not present, please run plot_characteristics_curves.py first to compute variances of predictions when the pressure varies inside and outside training interval")
    raise

file = open("data/predictedScalarsDataOODParam.pkl", 'rb')
predictedScalarsDataOODParam = pickle.load(file)
file.close()
predictedOutScalarsVarianceOODParam = predictedScalarsDataOODParam[
    "predictedOutScalarsVariance"]

logPredictedOutScalarsVarianceOODParam = {}
for sname in outScalarsNames:
    logPredictedOutScalarsVarianceOODParam[sname] = np.log10(
        predictedOutScalarsVarianceOODParam[sname])


if shutil.which('latex'):
    plt.rcParams.update({
        "text.usetex": True,
        "font.family": "sans-serif",
        "font.sans-serif": ["Helvetica"]})
mpl.style.use("seaborn-v0_8")

fontsize = 16
labelsize = 16
markersize = 6
markeredgewidth = 1

n_bins = 50
MAX = 15

fig, axs = plt.subplots(2, 2, sharey=True, tight_layout=True)

colors = [
    "tab:blue",
    "tab:orange",
    "tab:green",
    "tab:red",
    "tab:purple",
    "tab:brown",
    "tab:cyan"]
labels = [
    r"$\max_{\Gamma_{\rm top}}(u_2)$",
    r"$\max_\mathcal{M}(p)$",
    r"$\max_{\Gamma_{\rm top}}(\sigma_{22})$",
    r"$\max_\mathcal{M}(\mathrm{Von~Mises})$"]
markers = ["o", "s", "^", "P"]

# We can set the number of bins with the *bins* keyword argument.
for i in range(2):
    for j in range(2):
        I = i * 2 + j
        sname = outScalarsNames[I]
        axs[i, j].hist(logPredictedOutScalarsVarianceTest[sname],
                       bins=n_bins, color='darkgrey', label=labels[I])
        axs[i, j].plot([logPredictedOutScalarsVarianceOOD[sname][0], logPredictedOutScalarsVarianceOOD[sname][0]], [
                       0, MAX], linestyle="--", color='k')  # , label=nameCases[0])#marker=markers[0], markersize=markersize)
        axs[i, j].plot([logPredictedOutScalarsVarianceOOD[sname][1], logPredictedOutScalarsVarianceOOD[sname][1]], [
                       0, MAX], linestyle="-.", color='k')  # , label=nameCases[1])#marker=markers[1], markersize=markersize)
        axs[i,
            j].plot([logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   2],
                     logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   2],
                     logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   2]],
                    [0,
                     MAX / 2,
                     MAX],
                    linestyle="-",
                    color=colors[0],
                    linewidth=1.,
                    marker='<')  # , label=nameCases[1])#marker=markers[1], markersize=markersize)
        axs[i,
            j].plot([logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   3],
                     logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   3],
                     logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   3]],
                    [0,
                     MAX / 2,
                     MAX],
                    linestyle="-",
                    color=colors[1],
                    linewidth=1.,
                    marker='<')  # , label=nameCases[1])#marker=markers[1], markersize=markersize)
        axs[i,
            j].plot([logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   4],
                     logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   4],
                     logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   4]],
                    [0,
                     MAX / 2,
                     MAX],
                    linestyle="-",
                    color=colors[2],
                    linewidth=1.,
                    marker='o')  # , label=nameCases[1])#marker=markers[1], markersize=markersize)
        axs[i,
            j].plot([logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   5],
                     logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   5],
                     logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   5]],
                    [0,
                     MAX / 2,
                     MAX],
                    linestyle="-",
                    color=colors[3],
                    linewidth=1.,
                    marker='o')  # , label=nameCases[1])#marker=markers[1], markersize=markersize)
        axs[i,
            j].plot([logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   6],
                     logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   6],
                     logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   6]],
                    [0,
                     MAX / 2,
                     MAX],
                    linestyle="-",
                    color=colors[4],
                    linewidth=1.,
                    marker='o')  # , label=nameCases[1])#marker=markers[1], markersize=markersize)
        axs[i,
            j].plot([logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   7],
                     logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   7],
                     logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   7]],
                    [0,
                     MAX / 2,
                     MAX],
                    linestyle="-",
                    color=colors[5],
                    linewidth=1.,
                    marker='>')  # , label=nameCases[1])#marker=markers[1], markersize=markersize)
        axs[i,
            j].plot([logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   8],
                     logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   8],
                     logPredictedOutScalarsVarianceOODParam[sname][2,
                                                                   8]],
                    [0,
                     MAX / 2,
                     MAX],
                    linestyle="-",
                    color=colors[6],
                    linewidth=1.,
                    marker='>')  # , label=nameCases[1])#marker=markers[1], markersize=markersize)

        handles, _ = axs[i, j].get_legend_handles_labels()
        axs[i, j].legend(fontsize=fontsize, columnspacing=0.65,
                         handletextpad=0.1, markerscale=2, loc='upper right')


nPointsPerCarac = 7
pressureBoundaries = [-60., -30.]
pressureInputs = pressureBoundaries[0] + (pressureBoundaries[1] - pressureBoundaries[0]) / (
    nPointsPerCarac - 1) * np.arange(nPointsPerCarac)


# lgd = fig.legend(hists, labels, fontsize=fontsize, columnspacing=0.65, handletextpad=0.1, markerscale=2, loc='upper right')
# lgd = fig.legend([None, r"ellipsoid", r"wedge"]+[str(p) for p in pressureInputs], loc='lower left', fontsize=fontsize, bbox_to_anchor=(-0.1,-0.07), ncol=3)
lgd = fig.legend([None, r"ellips.", r"wedge"] + ["P=" + str(int(p)) for p in pressureInputs],
                 loc='lower left', fontsize=fontsize, bbox_to_anchor=(0, -0.15), ncol=5)

axs[0, 0].set_ylabel(r"$\mathrm{counts}$", fontsize=fontsize)
axs[1, 0].set_ylabel(r"$\mathrm{counts}$", fontsize=fontsize)
axs[1, 1].set_xlabel(r"$\mathrm{log(variance)}$", fontsize=fontsize)
axs[1, 0].set_xlabel(r"$\mathrm{log(variance)}$", fontsize=fontsize)


fig.tight_layout()
# plt.xscale("log")
fig.savefig(
    "plots/OOD_detection_histo.png",
    dpi=600,
    format='png',
    bbox_extra_artists=(
        lgd,
    ),
    bbox_inches='tight')

print("histo plot done")
