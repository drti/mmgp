# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#
#
import os
import pickle

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
import shutil

from plaid.containers.dataset import Dataset

from mmgp.processing import read_proj_in_coord_fields
from mmgp.utils import remove_file, read_configuration, read_problem

"""
Fig.7

In this file, the code for the bissect graph and the loading characteristics plots
for the 2D solid mechanics case is given (Figures 10 and 11).

Notice that the loading characteristics plots, no precomputation data is used, and the
complete inference workflow is provided and commented.
"""


# Clean previous plots
if os.path.isdir("plots") == False:
    os.mkdir("plots")
else:
    remove_file("plots/caracs_curves_saved_model_u_2.png")
    remove_file("plots/caracs_curves_saved_model_VonMises.png")


# Matplotlib plotting options
if shutil.which('latex'):
    plt.rcParams.update({
        "text.usetex": True,
        "font.family": "sans-serif",
        "font.sans-serif": ["Helvetica"]})
mpl.style.use("seaborn-v0_8")

fontsize = 32
labelsize = 32
markersize = 24
markeredgewidth = 1

# Load data
configuration = read_configuration(config_file="configuration.yaml")
problem = read_problem(configuration)

if configuration["regression"]["uncertainties"]:
    regressor_num = configuration["regression"]["reference_regressor"]
    nMonteCarloSamples = configuration["regression"]["number_Monte_Carlo_samples"]
else:
    regressor_num = 0
    nMonteCarloSamples = 1


generated_data_folder = configuration["generated_data_folder"]
# Load the data constructing during the training workflow
file = open(generated_data_folder+os.sep+"trainingRegressionData.pkl",'rb')
trainingRegressionData = pickle.load(file)
file.close()

scalarsRegressorsList = trainingRegressionData["scalarsRegressorsList"]
podProjInCoordFields = trainingRegressionData["podProjInCoordFields"]
scalerInScalar = trainingRegressionData["scalerInScalar"]
scalerX = trainingRegressionData["scalerX"]
scalerYScalars = trainingRegressionData["scalerYScalars"]

scalarsRegressors = scalarsRegressorsList[regressor_num]


#### Characteristic curve plots ####


# Specify input meshes and pressure
nMeshes = 4
nPointsPerCarac = 11
pressureBoundaries = [-70., -20.]
pressureInputs = pressureBoundaries[0] + (pressureBoundaries[1] - pressureBoundaries[0]) / (
    nPointsPerCarac - 1) * np.arange(nPointsPerCarac)

inScalarsRef = np.array([1500., 1500., 450., 0., 15., 7.5e4])

test_set_name = configuration["test_set"]
testing_set = problem.get_split(test_set_name)
ranks = np.take(testing_set, [100, 149, 139, 158])

morph_proj_dataset = Dataset()
folder = generated_data_folder + os.sep + configuration["case_name"] + "_morphed_and_projected/dataset"
morph_proj_dataset._load_from_dir_(folder, ids=ranks, verbose=True)

outScalarsNames = problem.out_scalars_names

# Initialize scalar prediction, variance and quantile arrays
predictedOutScalars = {}
predictedOutScalarsVariance = {}
for sname in outScalarsNames:
    predictedOutScalars[sname] = np.empty((nMeshes, nPointsPerCarac))
    predictedOutScalarsVariance[sname] = np.empty((nMeshes, nPointsPerCarac))


zone_name = configuration['zone_name']
base_name = configuration['base_name']


print("Plotting characteristics plot:")
for i in tqdm(range(len(ranks))):

    index = ranks[i]

    projInCoordFields = read_proj_in_coord_fields(
        morph_proj_dataset[index], zone_name, base_name)

    reducedProjInCoordFields = podProjInCoordFields.transform(
        projInCoordFields)

    for l in range(nPointsPerCarac):

        inScalars = np.copy(inScalarsRef)
        inScalars[3] = pressureInputs[l]

        # Rescale the nongeometrical input scalars
        rescaledInScalar = scalerInScalar.transform(
            inScalars.reshape(1, -1))[0, :]

        # Create the low-dimensional input by concatenating the two previous
        # inputs
        X = np.hstack(
            (reducedProjInCoordFields, rescaledInScalar)).reshape(
            1, -1)

        # Rescale the input
        rescaledX = scalerX.transform(X)

        predictedProjOutScalarSamples = {}
        for j, sname in enumerate(outScalarsNames):
            # Evaluate the scalar regressions
            rescaledYScalars = scalarsRegressors[sname].predict(
                rescaledX.reshape(1, -1))[0]
            rescaledYScalarsSamples = scalarsRegressors[sname].predict_Monte_Carlo_draws(
                rescaledX.reshape(1, -1), size=nMonteCarloSamples)

            # Inverse rescale the scalar outputs
            predictedOutScalars[sname][i, l] = scalerYScalars[sname].inverse_transform(
                rescaledYScalars.reshape(-1, 1))[:, 0]
            predictedProjOutScalarSamples[sname] = np.empty(nMonteCarloSamples)
            for k in range(nMonteCarloSamples):
                predictedProjOutScalarSamples[sname][k] = scalerYScalars[sname].inverse_transform(
                    rescaledYScalarsSamples[:, k].reshape(-1, 1))[:, 0]

            # Compute variance and quantiles from the Monte Carlo draws
            predictedOutScalarsVariance[sname][i, l] = np.var(
                predictedProjOutScalarSamples[sname])


# Matplotlib instructions
colors = ["tab:red", "tab:blue", "tab:green", "k"]
labels = [r"$\mathrm{Geometry}$ $" + str(i) + "$" for i in range(nMeshes)]
markers = ["o", "s", "^", "P"]
fig1, ax = plt.subplots(1, 1, figsize=(8, 7))
fig2, bx = plt.subplots(1, 1, figsize=(8, 7))

for i in range(nMeshes):
    ax.errorbar(pressureInputs,
                predictedOutScalars['max_U2_top'][i,
                                                  :],
                yerr=1.96 * np.sqrt(predictedOutScalarsVariance['max_U2_top'][i,
                                                                              :]),
                color=colors[i],
                label=labels[i],
                marker=markers[i])
    bx.errorbar(pressureInputs,
                predictedOutScalars['max_von_mises'][i,
                                                     :],
                yerr=1.96 * np.sqrt(predictedOutScalarsVariance['max_von_mises'][i,
                                                                                 :]),
                color=colors[i],
                label=labels[i],
                marker=markers[i])


y0 = ax.get_ylim()[0]
dy = 0.02 * (ax.get_ylim()[1] - y0)
ax.plot([-50., -40], [y0, y0], color="k", linestyle="-", linewidth=3)
ax.plot([-50., -50], [y0 - dy, y0 + dy], color="k", linestyle="-", linewidth=3)
ax.plot([-40., -40], [y0 - dy, y0 + dy], color="k", linestyle="-", linewidth=3)
ax.text(-58, y0 + 2 * dy, r'$\mathrm{training~interval}$', fontsize=fontsize)

y0 = bx.get_ylim()[0]
dy = 0.02 * (bx.get_ylim()[1] - y0)
bx.plot([-50., -40], [y0, y0], color="k", linestyle="-", linewidth=3)
bx.plot([-50., -50], [y0 - dy, y0 + dy], color="k", linestyle="-", linewidth=3)
bx.plot([-40., -40], [y0 - dy, y0 + dy], color="k", linestyle="-", linewidth=3)
bx.text(-58, y0 + 2 * dy, r'$\mathrm{training~interval}$', fontsize=fontsize)

ax.legend(
    fontsize=28,
    bbox_to_anchor=(
        0.5,
        1.3),
    loc='upper center',
    ncols=2,
    columnspacing=0.65,
    handletextpad=0.1,
    markerscale=2)
bx.legend(
    fontsize=28,
    bbox_to_anchor=(
        0.5,
        1.3),
    loc='upper center',
    ncols=2,
    columnspacing=0.65,
    handletextpad=0.1,
    markerscale=2)

xticks = np.arange(np.min(pressureInputs), np.max(pressureInputs) + 0.1, 10.0)
ax.set_xticks(xticks)
ax.set_xticklabels([r"$" + str(int(x)) + "$" for x in xticks])
bx.set_xticks(xticks)
bx.set_xticklabels([r"$" + str(int(x)) + "$" for x in xticks])
ax.tick_params(labelsize=labelsize)
bx.tick_params(labelsize=labelsize)

ax.set_xlabel(r"$\mathrm{Pressure}$", fontsize=fontsize)
bx.set_xlabel(r"$\mathrm{Pressure}$", fontsize=fontsize)
ax.set_ylabel(r"$\max_{\Gamma_{\rm top}}(u_2)$", fontsize=fontsize)
bx.set_ylabel(r"$\max_\mathcal{M}(\mathrm{Von~Mises})$", fontsize=fontsize)


fig1.savefig(
    "plots/caracs_curves_saved_model_u_2.png",
    format="png",
    bbox_inches="tight")
fig2.savefig(
    "plots/caracs_curves_saved_model_VonMises.png",
    format="png",
    bbox_inches="tight")


predictedScalarsData = {}
predictedScalarsData["predictedOutScalarsVariance"] = predictedOutScalarsVariance

with open("data/predictedScalarsDataOODParam.pkl", 'wb') as file:
    pickle.dump(predictedScalarsData, file)
