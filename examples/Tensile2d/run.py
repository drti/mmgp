# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#
#
import os
import time
import argparse
from mmgp.utils import read_configuration, read_problem

# python run.py --preprocess --train --infer --scalar_plots --complete_example_inference --export_predictions

parser = argparse.ArgumentParser()
parser.add_argument("--preprocess", action="store_true", help="Perform the preprocessing steps morphing and finite element interpolation")
parser.add_argument("--train", action="store_true", help="Train the latent Gaussian process")
parser.add_argument("--infer", action="store_true", help="Predict with the trained model")
parser.add_argument("--scalar_plots", action="store_true", help="Plot some results")
parser.add_argument("--complete_example_inference", action="store_true", help="Run the complete inference example")
parser.add_argument("--export_predictions", action="store_true", help="Export the predictions in PLAID format")
args = parser.parse_args()


print("----------------------------------------------")
print(" MMGP workflow")
print("----------------------------------------------")

configuration = read_configuration(config_file="configuration.yaml")
problem = read_problem(configuration)

if args.preprocess:
    from mmgp.preprocessing import pre_process
    print(" Preprocess starting ...")
    start = time.time()

    fe_interpolation_operators = pre_process(configuration)

    print("... preprocess done in "+str(time.time() - start)+" s")
else:
    print(" Preprocess bypassed")


print("----------------------------------------------")


if args.train:
    from mmgp.processing import train
    print(" Train starting ...")
    start = time.time()

    train_data = train(configuration, problem)

    print("... train done in "+str(time.time() - start)+" s")
else:
    print(" Train bypassed")


print("----------------------------------------------")


if args.infer:
    from mmgp.processing import infer
    print(" Inference starting ...")
    start = time.time()

    predicted_data = infer(configuration, problem)

    print("... inference done in "+str(time.time() - start)+" s")
else:
    print(" Inference bypassed")

print("----------------------------------------------")
print(" Some plots")
print("----------------------------------------------")


if args.scalar_plots:
    print(" Plotting scalars ...")
    start = time.time()

    os.system("python plot_characteristics_curves.py")
    os.system("python plot_OOD_detection.py")

    print("... scalar plotted in "+str(time.time() - start)+" s")
else:
    print(" Plotting scalars bypassed")

print("----------------------------------------------")
print(" Example of complete inference (with morphing and FE interpolation stages)")
print("----------------------------------------------")


if args.complete_example_inference:
    print(" Computing complete inference example ...")
    start = time.time()

    os.system("python complete_example_inference.py")

    print("... complete inference example computed in "+str(time.time() - start)+" s")
else:
    print(" Computing complete inference example bypassed")


print("----------------------------------------------")
print(" Export of the predictions in PLAID format")
print("----------------------------------------------")


if args.export_predictions:
    from mmgp.postprocessing import export_predictions
    print(" Exporting predictions ...")
    start = time.time()

    predicted_dataset = export_predictions(configuration, problem)

    print("... predictions exported in "+str(time.time() - start)+" s")
else:
    print(" Exporting predictions bypassed")


print("----------------------------------------------")
