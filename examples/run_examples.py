# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#
#
import os
from subprocess import call
import argparse


examples_to_run = [
    'SimpleTest']

# python run_examples.py --backend scikit-learn




parser = argparse.ArgumentParser()
parser.add_argument("--backend", nargs='?', const='scikit-learn', type=str, help="Choose a GP backend, default scikit-learn")
args = parser.parse_args()

if args.backend:
    backend = args.backend
else:
    backend = 'scikit-learn'

for ex_name in examples_to_run:
    print("testing example "+ex_name)
    os.chdir(ex_name)
    call(["python", "run.py", "--preprocess", "--train", backend, "--infer", "--compute_metrics", "--export_predictions"])
    print("==")
    os.chdir("..")
