from setuptools import setup

if __name__ == "__main__":

    setup()


# from setuptools import find_packages, setup

# setup(
#     name="mmgp",
#     version="0.1",
#     description="Learning physics-based simulations with mesh morphing, finite element interpolation, and Gaussian processes",
#     package_dir={"": "src"},
#     packages=find_packages(where="src"),
#     author="Safran",
#     author_email="fabien.casenave@safrangroup.com",
#     include_package_data=True,
#     python_requires=">=3.11",
# )
