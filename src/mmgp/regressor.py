# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#
#
import numpy as np
from typing import Self

class RegressorBase(object):
    """This class implements a regression model for different algorithms
    """

    def __init__(self):
        """Initialize the Regressor with the specified algorithm and options.

        Args:
            algo (str): The regression algorithm to use.
            options (dict): A dictionary of options specific to the chosen algorithm. Allowed fields are "kernel", "optim", "num_restarts", "max_iters" and "anisotropic".

        Raises:
            AssertionError: If the specified algorithm is not supported.

        Example:
            .. code-block:: python

                from mmgp.regressor import Regressor
                options = { 'kernel': 'Matern52', 'optim': 'bfgs', 'num_restarts': 1, 'max_iters': 1000, 'anisotropic': True }
                regressor = Regressor('GPy', options)
        """
        self.input_dim = None
        self.output_dim = None

        self.options = None
        self.algo = None

        self.kmodel = None

    def fit(self, X: np.ndarray, y: np.ndarray) -> Self:
        """Train the regression model on the provided data.

        Args:
            X (np.ndarray): The input features for training.
            y (np.ndarray): The target values for training.
        """
        raise NotImplementedError

    def predict(self, X: np.ndarray) -> np.ndarray:
        """Make predictions using the trained regression model.

        Args:
            X (np.ndarray): The input features for making predictions.

        Returns:
            np.ndarray: Predicted target values.
        """
        raise NotImplementedError


    def predict_Monte_Carlo_draws(
            self, X: np.ndarray, size: int = 100) -> np.ndarray:
        """Generate Monte Carlo draws from the trained regression model.

        Args:
            X (np.ndarray): The input features for generating draws.
            size (int, optional): The number of Monte Carlo draws to generate. Defaults to 100.

        Returns:
            np.ndarray: Monte Carlo draws from the posterior of the regression model.
        """
        raise NotImplementedError
