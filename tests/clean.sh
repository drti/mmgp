#!/bin/bash

# Search diff_*.json files
verbose=0

if [ ! -z "$1" ]; then
    if [ "$1" = "-v" ]; then
        verbose=1
    else
        echo "clean.sh: try -v to set verbose mode"
        exit 1
    fi
fi

log() {
    if [ $verbose -eq 1 ]; then
        echo $1
    fi
}

delete_json_diff_files() {
    diff_files=($(find ./$1 -type f -name "diff_*.json"))
    length="${#diff_files[*]}"

    if [ $length -eq 0 ]; then
        log "There is no file \"diff_*.json\" to delete."
    else
        VAR=0
        log "Starts deleting $length file(s)..."

        for file in "${diff_files[@]}"; do
            VAR=$(($VAR + 1))
            log "$VAR/$length deleting $file."
            rm "$file"
        done

        log "Deletion completed. $length file(s) deleted."
    fi
}

delete_dir_secure() {
    directory="$1"
    if [ -d "$directory" ]; then
        delete_json_diff_files "$directory"
        log ""
        echo "Deleting \"$directory/\" directory (secure)..."
        if [ -n "$(ls -A "$directory")" ]; then
            echo "Directory \"$directory/\" is not empty. Abort."
            exit 1
        else
            rmdir $directory
            log "Directory \"$directory/\" has been deleted."
        fi
    else
        log "There is no \"$directory/\" directory to delete."
    fi
}

delete_dir_force() {
    directory="$1"
    if [ -d "$directory" ]; then
        log ""
        echo "Deleting \"$directory/\" directory (force)..."
        if [ $verbose -eq 1 ]; then
            rm -v -rf $directory
        else
            rm -rf $directory
        fi

        log "Directory \"$directory/\" has been deleted."
    else
        log "There is no \"$directory/\" directory to delete."
    fi
}

delete_dir_force ".pytest_cache"
delete_dir_force "working_data"
delete_dir_force "new_working_test_data"
delete_dir_secure "diff"