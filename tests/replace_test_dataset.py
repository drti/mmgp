# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#
#
import os
import time
import numpy as np

from mmgp.utils import read_configuration, read_problem
from mmgp.preprocessing import pre_process

name_method = ["sklearn_0","sklearn_3000","GPy_0","GPy_3000"]

def reference_data(name_method):
    configuration = read_configuration("test_configuration_"+name_method+".yaml")
    configuration["generated_data_folder"] = "new_working_test_data/data_"+name_method

    problem = read_problem(configuration)
    
    print(" Preprocess starting ...")
    start = time.time()
    pre_process(configuration)
    print("... preprocess done in "+str(time.time() - start)+" s")


    from mmgp.processing import train
    print(" Train starting ...")
    start = time.time()
    np.random.seed(42)
    train(configuration, problem)
    print("... train done in "+str(time.time() - start)+" s")


    from mmgp.processing import infer
    print(" Inference starting ...")
    start = time.time()
    infer(configuration, problem)
    print("... inference done in "+str(time.time() - start)+" s")


    from mmgp.postprocessing import compute_metrics
    print(" Computation of metrics starting ...")
    start = time.time()
    compute_metrics(configuration, problem)
    print("... computation of metrics done in "+str(time.time() - start)+" s")
    
    
for name in name_method:
    reference_data(name)