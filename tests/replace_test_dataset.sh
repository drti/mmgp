#!/bin/bash

echo -n "Do you want to build the reference test database? (Y/N): "
read confirmation

if [ "$confirmation" != "Y" ] && [ "$confirmation" != "y" ]; then
    echo "Task aborted."
    exit 0
fi

echo "Starting to build the new database reference..."
python "replace_test_dataset.py"

if [ $? -ne 0 ]; then
    echo "Problem in python script need to be resolved. Task aborted."
    exit 1
fi

echo -n "Do you really want to replace the reference test database? (Y/N): "
read confirmation

if [ "$confirmation" != "Y" ] && [ "$confirmation" != "y" ]; then
    echo "Task aborted."
    exit 0
fi

echo "Replacing reference test database..."
rm -rf "working_test_data"
mv "new_working_test_data" "working_test_data"
echo "...reference test database replaced"

echo -n "Do you want additionnal information? (Y/N): "
read confirmation

if [ "$confirmation" != "Y" ] && [ "$confirmation" != "y" ]; then
    echo "Task aborted."
    exit 0
fi

echo -e "use this commit name: \"fix(working_test_data/): replacing tests reference dataset by a new version\""