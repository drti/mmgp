# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#
#

import pytest
from plaid.problem_definition import ProblemDefinition

from mmgp.utils import read_configuration, read_problem

@pytest.fixture(params=["sklearn_0", "sklearn_3000", "GPy_0", "GPy_3000"])
def name_method(request):
    return request.param

@pytest.fixture()
def configuration(name_method):
    return read_configuration(config_file="test_configuration_"+name_method+".yaml")

@pytest.fixture()
def problem(configuration):
    return read_problem(configuration)

class TestLoadingConfiguration():

    def test_read_configuration(self, configuration):
        assert (isinstance(configuration, dict))
        print(f"CONFIG:{configuration}")

    def test_read_problem(self, problem):
        assert (isinstance(problem, ProblemDefinition))
        #print(f"PROBLEM:{problem}")
