# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#
#

import pytest
from test04_preprocessing import name_method, configuration, medianMesh

from mmgp.morphing import Morphing


@pytest.fixture()
def morphing():
    return Morphing("Floater", {})

# def test_RenumberMeshForParametrization(inMesh, inPlace, boundaryOrientation, fixedBoundaryPoints, startingPointRankOnBoundary):
#     RenumberMeshForParametrization(inMesh, inPlace, boundaryOrientation, fixedBoundaryPoints, startingPointRankOnBoundary)

# def test_FloaterMeshParametrization(inMesh, nBoundary, outShape, boundaryOrientation, curvAbsBoundary, fixedInteriorPoints, fixedBoundaryPoints):
#     FloaterMeshParametrization(inMesh, nBoundary, outShape, boundaryOrientation, curvAbsBoundary, fixedInteriorPoints, fixedBoundaryPoints)


class TestMorphing():

    def test___init__(self, morphing):
        pass

    def test_transform(self, morphing, medianMesh):
        morphing.transform(medianMesh)
