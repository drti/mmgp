# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#
#
import glob
import json
import os
import pickle
import shutil

import numpy as np
import pytest
from Muscat.Bridges.CGNSBridge import CGNSToMesh
from deepdiff import DeepDiff
from plaid.containers.dataset import Dataset
from plaid.containers.sample import Sample
from test01_data_handler import configuration, name_method

from mmgp.morphing import Morphing
from mmgp.preprocessing import pre_process, pretreat_sample


@pytest.fixture()
def medianMesh(configuration):
    init_dataset_location = configuration['init_dataset_location']
    common_mesh_index = configuration["common_mesh_index"]

    # Load Dataset
    dataset = Dataset()
    dataset._load_from_dir_(
        os.path.join(
            init_dataset_location, "dataset"),
        ids=[common_mesh_index],
        verbose=False)
    cgns_mesh = dataset[common_mesh_index].get_mesh()
    median_mesh = CGNSToMesh(cgns_mesh)

    return median_mesh


@pytest.fixture()
def compute_common_morphed_mesh(configuration, medianMesh):
    morphing = Morphing(
        algo=configuration["morphing"]["algo"],
        options=configuration["morphing"]["options"])
    commonMorphedMesh = morphing.transform(medianMesh)

    return commonMorphedMesh

@pytest.fixture()
def ref_data(name_method):
    return "working_test_data/data_"+name_method

@pytest.fixture()
def gen_data(name_method):
    return "working_data/data_"+name_method


def create_directory(directory_path='diff'):
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)

def get_sorted_directories(
        home_path: str, specific_path: str, pattern_path: str) -> list[str]:
    return sorted([path for path in glob.glob(os.path.join(
        home_path, specific_path, pattern_path)) if os.path.isdir(path)])


def get_sorted_files(home_path: str, specific_path: str,
                     pattern_path: str) -> list[str]:
    return sorted([path for path in glob.glob(os.path.join(
        home_path, specific_path, pattern_path)) if os.path.isfile(path)])


def compare_samples(options: dict[str:str]):
    sample_A, sample_B = Sample(), Sample()
    sample_A.load(options['first_path'])
    sample_B.load(options['second_path'])
    diff = DeepDiff(sample_A, sample_B, math_epsilon=0.000001)

    if diff:
        create_directory()
        filename = f"diff/diff_{options['stage_name']}.json"

        with open(filename, 'w') as file:
            json.dump(diff.to_dict(), file, indent=4)
        assert False, f"Failed regression test for the stage: {options['stage_name']}. Look at file '{filename}' to see the differences."


def pick_elements(options: dict[str, str]):
    with open(options['first_path'], 'rb') as file:
        f_elt = pickle.load(file)
    with open(options['second_path'], 'rb') as file:
        s_elt = pickle.load(file)
    return f_elt, s_elt


def compare_sparse_matrix(options: dict[str, str], key: str = ''):
    arr_1, arr_2 = pick_elements(options)

    if len(key) != 0:
        arr_1, arr_2 = arr_1[key], arr_2[key]

    equal = np.allclose(arr_1.toarray(), arr_2.toarray(), atol=0.000001)

    if not equal:
        assert False, f"Failed regression test for the stage: {options['stage_name']}."


def compare_custom_objects(options: dict[str, str], key: str = ''):
    first_loaded_data, second_loaded_data = pick_elements(options)

    if len(key) != 0:
        diff = DeepDiff(
            first_loaded_data[key],
            second_loaded_data[key],
            math_epsilon=0.000001)
    else:
        diff = DeepDiff(
            first_loaded_data,
            second_loaded_data,
            math_epsilon=0.000001)

    if diff:
        create_directory()
        filename = f"diff/diff_{options['stage_name']}.json"
        with open(filename, 'w') as file:
            json.dump(diff.to_dict(), file, indent=4)
        assert False, f"Failed regression test for the stage: {options['stage_name']}. Look at file '{filename}' to see the differences."


def compare_loaded_type(stage_name: str, paths_list: zip,
                        type: str = 'object', key: str = ''):
    if type == 'sample':
        for elt1, elt2 in paths_list:
            compare_samples({'stage_name': stage_name,
                            'first_path': elt1, 'second_path': elt2})
    elif type == 'object':
        for elt1, elt2 in paths_list:
            compare_custom_objects(
                {'stage_name': stage_name, 'first_path': elt1, 'second_path': elt2}, key)
    elif type == 'numpy':
        for elt1, elt2 in paths_list:
            compare_sparse_matrix(
                {'stage_name': stage_name, 'first_path': elt1, 'second_path': elt2}, key)
    else:
        assert False, "The two types to compare are not supported"


class TestPreProcessing():
    
    def test_pre_process_exec(self, configuration):
        results = pre_process(configuration)
        
    def test_pre_process_morphed(self, ref_data, gen_data):
        paths_to_check_A = get_sorted_directories(
            gen_data, "TestCase_morphed/dataset/samples", "sample_*")
        paths_to_check_B = get_sorted_directories(
            ref_data, "TestCase_morphed/dataset/samples", "sample_*")
        assert (len(paths_to_check_A) == len(paths_to_check_B))
        assert (len(paths_to_check_A) == 3)
        stage_name = 'comparison_morphed_mesh_and_transported_fields'
        compare_loaded_type(
            stage_name, zip(
                paths_to_check_A, paths_to_check_B), type='sample')
        
    def test_pre_process_common_morphed_and_projected(
            self, ref_data, gen_data):
        paths_to_check_A = get_sorted_directories(
            gen_data, "TestCase_morphed_and_projected/dataset/samples", "sample_*")
        paths_to_check_B = get_sorted_directories(
            ref_data, "TestCase_morphed_and_projected/dataset/samples", "sample_*")
        assert (len(paths_to_check_A) == len(paths_to_check_B))
        assert (len(paths_to_check_A) == 3)
        stage_name = "comparison_common_morphed_mesh_and_projected_coordinate_fields_and_fields_of_interest"
        compare_loaded_type(
            stage_name, zip(
                paths_to_check_A, paths_to_check_B), type='sample')
        
    def test_pre_process_precomputed_FE_projection(self, ref_data, gen_data):
        paths_to_check_A = get_sorted_files(
            gen_data, "FEInterpolationOperators", "sample_*")
        paths_to_check_B = get_sorted_files(
            ref_data, "FEInterpolationOperators", "sample_*")
        assert (len(paths_to_check_A) == len(paths_to_check_B))
        assert (len(paths_to_check_A) == 3)
        stage_name = "comparison_precomputed_FE_projection_operator"
        compare_loaded_type(
            stage_name, zip(
                paths_to_check_A, paths_to_check_B), type='numpy', key="projOperator")
        
    def test_pre_process_precomputed_inv_FE_projection(
            self, ref_data, gen_data):
        paths_to_check_A = get_sorted_files(
            gen_data, "FEInterpolationOperators", "sample_*")
        paths_to_check_B = get_sorted_files(
            ref_data, "FEInterpolationOperators", "sample_*")
        assert (len(paths_to_check_A) == len(paths_to_check_B))
        assert (len(paths_to_check_A) == 3)
        stage_name = "comparison_precomputed_inverse_FE_projection operator"
        compare_loaded_type(
            stage_name, zip(
                paths_to_check_A, paths_to_check_B), type='numpy', key="invProjOperator")
    
    def test_pretreat_sample(self, configuration, compute_common_morphed_mesh):
        generated_data_folder = configuration['generated_data_folder']
        case_name = "isolated_pretreat_sample_test"

        special_configuration = {
            'case_name': case_name,
            'base_name': configuration["base_name"],
            'zone_name': configuration["zone_name"],
            'morphing': {
                'algo': configuration["morphing"]["algo"],
                'options': configuration["morphing"]["options"]
                },
            'init_dataset_location': configuration['init_dataset_location'],
            'generated_data_folder': generated_data_folder
        }

        # Delete if precedant usage of tests
        path = os.path.join(generated_data_folder, case_name + "_morphed")
        if os.path.exists(path) and os.path.isdir(path):
            shutil.rmtree(path)

        path = os.path.join(
            generated_data_folder,
            case_name +
            "_morphed_and_projected")
        if os.path.exists(path) and os.path.isdir(path):
            shutil.rmtree(path)

        i_sample = 0

        pretreat_sample(
            special_configuration,
            compute_common_morphed_mesh,
            i_sample)
