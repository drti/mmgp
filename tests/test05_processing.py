# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#
#

import json
import os
import pytest

import numpy as np
from deepdiff import DeepDiff
from test01_data_handler import name_method, configuration, problem
from test04_preprocessing import gen_data, ref_data, pick_elements, create_directory

from mmgp.processing import infer, train

# Global Data to store training regression data from the 'train' function
data_1 = None
data_2 = None

@pytest.fixture(params=["sklearn", "GPy"])
def name_model(request):
    return request.param

@pytest.fixture()
def ref_data_0(name_model):
    return "working_test_data/data_"+name_model+"_0"

@pytest.fixture()
def ref_data_3000(name_model):
    return "working_test_data/data_"+name_model+"_3000"

@pytest.fixture()
def gen_data_0(name_model):
    return "working_data/data_"+name_model+"_0"

@pytest.fixture()
def gen_data_3000(name_model):
    return "working_data/data_"+name_model+"_3000"


def compare_sparse_matrix(e_1, e_2):
    if e_1.shape != e_2.shape:
        assert False, "Failed regression test for the stage DimensionalityReductor: comparing correlation operator of dimensionality reductor."

    diff = np.abs(e_1 - e_2)
    equal = np.all(diff.data < 0.000001)

    if not equal:
        assert False, "Failed regression test for the stage DimensionalityReductor: comparing correlation operator of dimensionality reductor."


def compare_array(e_1, e_2):
    diff = np.abs(e_1 - e_2)
    equal = len(diff[diff > 0.000001]) == 0

    if not equal:
        assert False, "Failed regression test for the stage DimensionalityReductor: comparing reduced order basis of dimensionality reductor."


def compare_dim_reductor(e_1, e_2):
    e_1, e_2 = e_1.training_data, e_2.training_data
    compare_sparse_matrix(
        e_1['correlationOperator'],
        e_2['correlationOperator'])
    compare_array(e_1['reducedOrderBasis'], e_2['reducedOrderBasis'])


def compare_dict_dim_reductor(e_1, e_2):
    for key in e_1:
        compare_dim_reductor(e_1[key], e_2[key])


def compare_custom_obj(e_1, e_2, stage_name, threshold=0.000001):
    diff = DeepDiff(e_1, e_2, math_epsilon=threshold)

    if diff:
        create_directory()
        filename = f"diff/diff_{stage_name}_{e_1.__class__.__name__}.json"
        with open(filename, 'w') as file:
            json.dump(diff.to_dict(), file, indent=4)
        assert False, f"Failed regression test for the stage: {stage_name}. Look at file '{filename}' to see the differences."

    if diff:
        create_directory()
        filename = f"diff/diff_{stage_name}_{e_1.__class__.__name__}.json"
        with open(filename, 'w') as file:
            json.dump(diff.to_dict(), file, indent=4)
        assert False, f"Failed regression test for the stage: {stage_name}. Look at file '{filename}' to see the differences."


def compare_dict_custom_obj(e_1, e_2, stage_name):
    for key in e_1:
        compare_custom_obj(e_1[key], e_2[key], stage_name)


class TestProcessing():

    def test_train(self, configuration, problem):
        # init_dataset is reset
        # morph_proj_dataset is reset
        np.random.seed(42)
        train(configuration, problem)

    def test_read_training_regression_data(self, ref_data, gen_data):
        global data_1, data_2
        paths_to_check_A = os.path.join(gen_data, "trainingRegressionData.pkl")
        paths_to_check_B = os.path.join(ref_data, "trainingRegressionData.pkl")

        options = {
            'first_path': paths_to_check_A,
            'second_path': paths_to_check_B
        }

        data_1, data_2 = pick_elements(options)
        assert len(data_1) == len(data_2)
        assert len(
            data_1) != 0, "Probleme in test suite execution or problem detected in code"
        assert set(data_1.keys()) == set(data_2.keys())

    def test_train_pod_proj_in_coord_fields(self):
        global data_1, data_2
        compare_dim_reductor(
            data_1["podProjInCoordFields"],
            data_2["podProjInCoordFields"])

    def test_train_scaler_in_scalar(self):
        global data_1, data_2
        compare_custom_obj(
            data_1["scalerInScalar"],
            data_2["scalerInScalar"],
            "scaler_in_scalar")

    def test_train_scaler_X(self):
        global data_1, data_2
        compare_custom_obj(data_1["scalerX"], data_2["scalerX"], "scaler_X")

    def test_train_pod_proj_out_fields(self):
        global data_1, data_2
        compare_dict_dim_reductor(
            data_1["podProjOutFields"],
            data_2["podProjOutFields"])

    def test_train_scaler_Y_fields(self):
        global data_1, data_2
        compare_dict_custom_obj(
            data_1["scalerYFields"],
            data_2["scalerYFields"],
            "scaler_Y_fields")

    def test_train_scaler_y_scalars(self):
        global data_1, data_2
        compare_dict_custom_obj(
            data_1["scalerYScalars"],
            data_2["scalerYScalars"],
            "scaler_Y_scalars")

    def test_train_fields_regressors_list(self):
        # global data_1, data_2
        # list_data_1, list_data_2 = data_1["fieldsRegressorsList"], data_2["fieldsRegressorsList"]
        # for item_1, item_2 in zip(list_data_1, list_data_2):
        #    compare_dict_custom_obj(item_1, item_2, "fields_regressors_list")
        pass

    def test_train_scalars_regressors_list(self):
        # global data_1, data_2
        # for key in range(len(data_1["scalarsRegressorsList"])):
        #    compare_dict_custom_obj(data_1["scalarsRegressorsList"][key], data_2["scalarsRegressorsList"][key], "scalars_regressors_list")
        pass

    def test_infer(self, configuration, problem):
        infer(configuration, problem)

    def test_infer_predicted_data_0(self, gen_data_0, ref_data_0):
        paths_to_check_A = os.path.join(gen_data_0, "allPredictedData.pkl")
        paths_to_check_B = os.path.join(ref_data_0, "allPredictedData.pkl")
        options = {
            'first_path': paths_to_check_A,
            'second_path': paths_to_check_B}
        data_1, data_2 = pick_elements(options)
        assert len(data_1) == len(data_2)
        assert len(
            data_1) != 0, "Probleme in test suite execution or problem detected in code : "
        compare_custom_obj(data_1, data_2, "all_predicted_data")

    def test_infer_predicted_data_3000(self, gen_data_3000, ref_data_3000):
        paths_to_check_A = os.path.join(gen_data_3000, "allPredictedData.pkl")
        paths_to_check_B = os.path.join(ref_data_3000, "allPredictedData.pkl")
        options = {
            'first_path': paths_to_check_A,
            'second_path': paths_to_check_B}
        data_1, data_2 = pick_elements(options)
        assert len(data_1) == len(data_2)
        assert len(
            data_1) != 0, "Probleme in test suite execution or problem detected in code : "
        data_1_mean = {k:data_1[k] for k in ["predictedOutFields","predictedOutScalars"]}
        data_2_mean = {k:data_2[k] for k in ["predictedOutFields","predictedOutScalars"]}
        compare_custom_obj(data_1_mean, data_2_mean, "mean")
        data_1_var_field=data_1["predictedOutFieldsVariance"]["field_1"]
        data_2_var_field=data_2["predictedOutFieldsVariance"]["field_1"]
        data_1_var_field_normalized=[data_1_var_field[i]/np.max(data_2_var_field[i]) for i in range(len(data_1_var_field))]
        data_2_var_field_normalized=[data_2_var_field[i]/np.max(data_2_var_field[i]) for i in range(len(data_2_var_field))]
        compare_custom_obj(data_1_var_field_normalized, data_2_var_field_normalized, "var_field", threshold=0.1)
        data_1_var_scalar=data_1["predictedOutScalarsVariance"]["scalar_2"]
        data_2_var_scalar=data_2["predictedOutScalarsVariance"]["scalar_2"]
        data_1_var_scalar_normalized=[i/max(data_2_var_scalar) for i in data_1_var_scalar]
        data_2_var_scalar_normalized=[i/max(data_2_var_scalar) for i in data_2_var_scalar]
        compare_custom_obj(data_1_var_scalar_normalized, data_2_var_scalar_normalized, "var_scalar", threshold=0.1)

    def test_infer_0_3000(self,gen_data_0,gen_data_3000):
        print(f"analytic:{gen_data_0}, MC:{gen_data_3000}")
        paths_to_check_A = os.path.join(gen_data_0, "allPredictedData.pkl")
        paths_to_check_B = os.path.join(gen_data_3000, "allPredictedData.pkl")
        options = {
            'first_path': paths_to_check_A,
            'second_path': paths_to_check_B}
        data_1, data_2 = pick_elements(options)
        assert len(data_1) == len(data_2)
        assert len(
            data_1) != 0, "Probleme in test suite execution or problem detected in code : "
        data_1_var_field=data_1["predictedOutFieldsVariance"]["field_1"]
        data_2_var_field=data_2["predictedOutFieldsVariance"]["field_1"]
        data_1_var_field_normalized=[data_1_var_field[i]/np.max(data_2_var_field[i]) for i in range(len(data_1_var_field))]
        data_2_var_field_normalized=[data_2_var_field[i]/np.max(data_2_var_field[i]) for i in range(len(data_2_var_field))]
        compare_custom_obj(data_1_var_field_normalized, data_2_var_field_normalized, "var_field", threshold=0.1)
        data_1_var_scalar=data_1["predictedOutScalarsVariance"]["scalar_2"]
        data_2_var_scalar=data_2["predictedOutScalarsVariance"]["scalar_2"]
        data_1_var_scalar_normalized=[i/max(data_2_var_scalar) for i in data_1_var_scalar]
        data_2_var_scalar_normalized=[i/max(data_2_var_scalar) for i in data_2_var_scalar]
        compare_custom_obj(data_1_var_scalar_normalized, data_2_var_scalar_normalized, "var_scalar", threshold=0.1)
