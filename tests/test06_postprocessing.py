# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#
#

import json
import os

import yaml
from deepdiff import DeepDiff
from test01_data_handler import name_method, configuration, problem
from test04_preprocessing import gen_data, ref_data, create_directory

from mmgp.postprocessing import compute_metrics


class TestPostProcessing():
    def test_transform(self, configuration, problem):
        # all_predicted_data is reset
        compute_metrics(configuration, problem)

    def test_metrics_output(self, gen_data, ref_data):
        with open(ref_data + os.sep + 'metrics.yaml', 'r') as yaml_file:
            yaml_ref_data = yaml.load(yaml_file, Loader=yaml.FullLoader)
        with open(gen_data + os.sep + 'metrics.yaml', 'r') as yaml_file:
            yaml_gen_data = yaml.load(yaml_file, Loader=yaml.FullLoader)

        diff = DeepDiff(yaml_ref_data, yaml_gen_data, math_epsilon=0.000001)

        if diff:
            create_directory()
            filename = "diff/diff_metrics.json"
            with open(filename, 'w') as file:
                json.dump(diff.to_dict(), file, indent=4)
            assert False, f"Failed regression test for the stage: compute metrics. Look at file '{filename}' to see the differences."
