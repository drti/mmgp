# %% Imports

import argparse
import glob
import os
import time

import numpy as np
from Muscat.Bridges.CGNSBridge import MeshToCGNS
from Muscat.IO.VtkReader import VtkReader
from plaid.containers.dataset import Dataset
from plaid.containers.sample import Sample
from plaid.problem_definition import ProblemDefinition

t0 = time.perf_counter()


print(f'Imports took: {time.perf_counter()-t0:.3f} s')

# %% Functions

# %% Classes

# %% Main Script
if __name__ == '__main__':
    # ---# Input parameters
    parser = argparse.ArgumentParser(description='',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-w', '--working_dir',
                        # dest='working_dir',
                        type=str,
                        default='.',
                        help='working directory',
                        )
    args = parser.parse_args()
    print()
    print(args)
    print()

    if args.working_dir[-1] == '/':
        args.working_dir = args.working_dir[:-1]

    raw_dir = os.path.join(args.working_dir, 'raw')
    dataset_dir = os.path.join(args.working_dir, 'dataset')
    if not (os.path.isdir(dataset_dir)):
        os.makedirs(dataset_dir)
    problem_definition_dir = os.path.join(
        args.working_dir, 'problem_definition')
    if not (os.path.isdir(problem_definition_dir)):
        os.makedirs(problem_definition_dir)

    vtk_samples_fnames = sorted(
        glob.glob(
            os.path.join(
                raw_dir,
                'sample_*.vtk')))

    # ---#
    samples = []
    for fname in vtk_samples_fnames:
        print()
        print(f"Read {fname}")

        BTmesh = VtkReader(fname).Read()
        BTmesh.nodes = BTmesh.nodes[:, :2]
        CGNSmesh = MeshToCGNS(BTmesh)

        sample = Sample()
        sample.add_tree(CGNSmesh)
        sample.show_tree()
        sample.add_scalar('scalar_1', np.random.randn())
        sample.add_scalar('scalar_2', np.random.randn())
        sample.add_field('field_1', np.random.randn(
            len(sample.get_nodes())), "Zone", "Base_2_2")

        samples.append(sample)

    # ---#
    infos = {
        "legal": {
            "owner": "Owner",
            "license": "Licence"},
        "data_production": {
            "type": "Test",
            "physics": "test data for mmgp library",
            "simulator": "test"}
    }

    # ---#
    dataset = Dataset()
    dataset.add_samples(samples)
    dataset.set_infos(infos)

    dataset._save_to_dir_(dataset_dir)

    # ---#
    split = {
        'train': [0, 1],
        'test': [2],
    }

    # ---#
    problem_definition = ProblemDefinition()
    problem_definition.add_input_mesh_name('mesh')
    problem_definition.add_input_scalar_name('scalar_1')
    problem_definition.add_output_scalar_name('scalar_2')
    problem_definition.add_output_field_name('field_1')
    problem_definition.set_task('regression')
    problem_definition.set_split(split)

    problem_definition._save_to_dir_(problem_definition_dir)
